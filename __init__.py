import sys
import hashlib

import rdflib
from rdflib import Graph, Literal, URIRef, BNode, Namespace
from rdflib.namespace import FOAF, RDF, DC, DOAP

from .githubpy.github import GitHub

try:
	from .secrets import gh_username, gh_access_token
except ImportError:
	gh_username = None
	gh_access_token = None

CV = Namespace('http://rdfs.org/resume-rdf/cv.rdfs#')
BIO = Namespace('http://vocab.org/bio/')

META = Namespace('http://pafcu.fi/ns/meta/')
def triple_uri(triple):
	return URIRef('triple:sha1:%s'%hashlib.sha1(bytes(str(triple),'utf-8')).hexdigest())

def get_profile(userid):
	g = rdflib.Graph()

	gh = GitHub(username=gh_username, access_token=gh_access_token)

	user = gh.users(userid).get()

	userurl = URIRef(user['url'])

	if user['name']: g.add((userurl, FOAF.name, Literal(user['name'])))
	if user['login']: g.add((userurl, FOAF.nick, Literal(user['login'])))
	if user['avatar_url']: g.add((userurl, FOAF.logo, Literal(user['avatar_url'])))
	if user['blog']: g.add((userurl, FOAF.weblog, Literal(user['blog'])))
	if user['bio']: g.add((userurl, CV.olb, Literal(user['bio'])))
	if user['email']: g.add((userurl, FOAF.mbox, URIRef('mailto:'+user['email'])))

	if user['company']:
		company = BNode()
		name = user['company']
		g.add((company, FOAF.name, Literal(name)))

	account = BNode()
	g.add((userurl, FOAF.account, account))
	g.add((account, FOAF.accountName, Literal('Github')))
	g.add((account, FOAF.accountServiceHomepage, URIRef('https://github.com/')))

	repos = gh.users(userid)('repos').get()

	for repo in repos:
		repourl = URIRef(repo['url'])
		g.add((repourl, RDF.type, DOAP.Project))
		if repo['name']: g.add((repourl, DOAP.name, Literal(repo['name'])))
		if repo['description']: g.add((repourl, DOAP.description, Literal(repo['description'])))
		if repo['homepage']: g.add((repourl, DOAP.homepage, URIRef(repo['homepage'])))
		if repo['language']: g.add((repourl, DOAP['programming-language'], Literal(repo['language'])))
		if repo['has_wiki']: g.add((repourl, DOAP.wiki, URIRef(repo['html_url']+'/wiki')))
		if repo['has_issues']: g.add((repourl, DOAP['bug-database'], URIRef(repo['html_url']+'/issues')))
		if repo['has_downloads']: g.add((repourl, DOAP['download-page'], URIRef(repo['html_url']+'/downloads')))

		gitrepo = BNode()
		g.add((gitrepo, RDF.type, DOAP.GitRepository))
		g.add((gitrepo, DOAP.location, URIRef(repo['git_url'])))
		g.add((gitrepo, DOAP.browse, URIRef(repo['html_url'])))

		releases = gh.repos(userid)(repo['name'])('releases').get()
		for release in releases:
			version = BNode()
			g.add((gitrepo,  DOAP.release, version))
			if release['name']: g.add((version, DOAP.name, Literal(release['name'])))
			if release['created_at']: g.add((version, DOAP.created, Literal(release['created_at'])))
			if release['tag_name']: g.add((version, DOAP.revision, Literal(release['tag_name'])))

		g.add((repourl, DOAP.maintainer, userurl))

	for triple in g:
		g.add((triple_uri(triple), META.sourceService, URIRef('https://github.com/')))

	return g
